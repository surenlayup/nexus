const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const { src, dest } = require('gulp');
const sourcemaps = require('gulp-sourcemaps');

// Nexus Project

// Compile sass into CSS & auto-inject into browsers
function style() {
    return gulp.src(['scss/style.scss'])
        .pipe(sass())
        .pipe(sourcemaps.init())
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest("./chapter-1/css"))
        .pipe(gulp.dest("./questions/css"))
        .pipe(gulp.dest("./chapter-2/css"))
        .pipe(browserSync.stream());
}

// Static Server + watching scss/html files
function watch() {

    browserSync.init({
        server: "./"
    });

    gulp.watch(['node_modules/bootstrap/scss/bootstrap.scss', 'scss/*.scss'], style);
    gulp.watch('**/*.html').on('change', browserSync.reload);
}

exports.style = style;
exports.watch = watch;
exports.default = watch;